﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HunterCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            double focus = 100;
            double rattackpower = 5312;
            double versatility = 452;
            double versdmg = 3.52;
            double crit = 954;
            double multistrike = 1007;
            double haste = 599;
            double agility = 4829;
            double stamina = 4903;
            double rWeaponDmg = 4713;
          

            double bWPetdmg = .2;       // pet 20% more dmg, Player +10% dmg , Shots 50% less focus.
            double bWPlayerdmg = .1;
            double bWtime = 10;
            double bWCD = 1;   //minutes
            double bwFocusCost = 0;
            double mOfBeast = 1;  // equals 1 if not active 1.18 if active

            double kCFocusCost = 40;
            double kCCD = 6;
            double kCGcd = 1;
            double kCDmg = (1.5 * (0 + rattackpower * 1.632) * mOfBeast * (1 + versdmg));

            double aSFocusCost = 30;
            double aSGcd = 1;
            double aSdmg = 1.25 * rWeaponDmg;

            double cSFocusCost = 0;
            double cSCastTime = 2;
            double cSCD = 0;
            double cSGcd = 1;
            double cSdmg = .75 * rWeaponDmg;

            double gcd = 1;

            //rotation

            int testTime = 120;  // in minutes

            double dps = 0;

            for (int i = testTime; i > 0; i--)
            {
                
                killCommand(ref focus, kCFocusCost, kCDmg, ref dps, kCGcd, ref gcd);
                beastialWrath();
                arcaneShot();
                cobraShot();

            }//end for

            Console.WriteLine("Your DPS is: " + dps);
        }//end main

        static void killCommand(ref double focus, double kCFocusCost, double kCDmg, ref double dps, double kCGcd, ref double gcd)
        {
            gcdCheck(gcd);
            focus = focus - kCFocusCost;
            dps = dps + kCDmg;
            gcd = kCGcd;
        }

        static void beastialWrath()
        {
            
        }

        static void arcaneShot()
        {

        }

        static void cobraShot()
        {

        }

        static void gcdCheck(double aGCD)
        {
            if (aGCD == 1)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

    }
}
